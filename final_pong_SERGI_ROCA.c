/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 1000;
    int screenHeight = 600;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    int playerSpeedY;
    
    Rectangle enemy;
    int enemySpeedY;
    
  
    InitAudioDevice();
    Music musicPong = LoadMusicStream("Dungeon_Boss.ogg");
    PlayMusicStream(musicPong);
    SetMusicVolume(musicPong, 0.3f); 
    int playerLife;
    int enemyLife;
    
    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
   
   Rectangle recCentral;
    recCentral.x = 0;
    recCentral.y = 0;
    recCentral.width = 40;
    recCentral.height =200;
    
    Rectangle recCentral2;
    recCentral2.x = 960;
    recCentral2.y = 0;
    recCentral2.width = 40;
    recCentral2.height = 200;
   
   
    int visionEnemy = screenWidth - 500;
    int speedEnemy = 3;
   
    Vector2 ballPosition = { screenWidth/2, screenHeight/2 };
    Vector2 ballSpeed = { 8, 9 };
    float ballRadius = 30;
    
 
    Color col = RED;
    int vida = 100;
    int vida2 = 100;
    int aceleracionY = 5;
    int aceleracionY2 = 8;
    bool pausa = false;
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                
                
                
                if (IsKeyPressed(KEY_ENTER))
                {
                   (screen) = TITLE;
                }
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                if (IsKeyPressed(KEY_ENTER))
                {
                    screen = GAMEPLAY;
                }
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                if (pausa == false)
                    {
                        if (IsKeyDown (KEY_P))
                        {
                            pausa = !pausa;
                        }
                        UpdateMusicStream(musicPong);
                        
                        if (IsKeyDown (KEY_S)) 
                        {
                        recCentral.y = recCentral.y + aceleracionY;
                        }
             
                        if (IsKeyDown (KEY_W))
                        {
                        recCentral.y = recCentral.y - aceleracionY;
                        }
                        
                        if((recCentral.y >= (screenHeight - recCentral.height) ) ) 
                        {   
                           recCentral.y = screenHeight- recCentral.height;
                        }  
                    
                        if (recCentral.y <= 0) 
                        {
                            recCentral.y = 0;
                        }
                    
                        //if (IsKeyDown (KEY_DOWN)) 
                        //{
                          // recCentral2.y = recCentral2.y + aceleracionY;
                        //}
                 
                        //if (IsKeyDown (KEY_UP))
                        //{
                          //  recCentral2.y = recCentral2.y - aceleracionY;
                        //}
                    
                        if((recCentral2.y >= (screenHeight - recCentral2.height) ) ) 
                        {   
                            recCentral2.y = screenHeight- recCentral2.height;
                        }  
                   
                        if (recCentral2.y <= 0) 
                        {
                            recCentral2.y = 0;
                        }
                  
                        
                        if (pausa == false)
                        {
                            ballPosition.x += ballSpeed.x;
                            ballPosition.y += ballSpeed.y;
                        }
                        if (((ballPosition.x + ballRadius) >= screenWidth))
                        {
                            (ballSpeed.x *= -1) && (vida2 = (vida2 - 10));
                        ;}
                        
                        if ((ballPosition.x - ballRadius) <= 0)
                        {
                            (ballSpeed.x *= -1) && (vida = (vida - 10));
                        }
                        
                        if (((ballPosition.y + ballRadius) >= screenHeight) || ((ballPosition.y - ballRadius) <= 0)) ballSpeed.y *= -1;
                        if (CheckCollisionCircleRec(ballPosition, ballRadius, recCentral2))
                        {
                           ballSpeed.x = -5;
                        }       
                        else if (CheckCollisionCircleRec(ballPosition, ballRadius, recCentral))
                        {           
                           ballSpeed.x = +5;
                        }
                        
                        if (ballPosition.x >= visionEnemy)
                        {
                            if (ballPosition.y > (recCentral2.y + recCentral2.height/2)) recCentral2.y += speedEnemy;
                            if (ballPosition.y < (recCentral2.y + recCentral2.height/2)) recCentral2.y -= speedEnemy;
                        }
                        if (IsKeyDown(KEY_RIGHT)) visionEnemy += 10;
                        if (IsKeyDown(KEY_LEFT)) visionEnemy -= 10;
                        
                        if (vida < 10)
                        {
                            screen = ENDING;
                        }
                        
                        if (vida2 < 10)
                        {
                            screen = ENDING;
                        }
                }// TODO: Ball movement logic.........................(0.2p)
                
                // TODO: Player movement logic.......................(0.2p)
                
                // TODO: Enemy movement logic (IA)...................(1p)
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)

                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                if (IsKeyPressed(KEY_R))
                {
                    screen = GAMEPLAY;
                }
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    DrawText ("Chusti Games Presents...", 0, 250, 80, Fade (BLUE, 0.5f));
                    DrawText("Press Enter to skip this shitty intro...", 0, 500, 30, (BLACK));
                    // TODO: Draw Logo...............................(0.2p)
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    DrawText("Budget Pong!", 200, 250, 90, RED);
                    DrawText("Press Enter to continue...", 0, 500, 30, BLACK);
                    // TODO: Draw Title..............................(0.2p)
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
           
                    DrawRectangleRec(recCentral, col);
                    DrawRectangleRec(recCentral2, col);
                    DrawCircleV(ballPosition, ballRadius, MAROON);
                    DrawLine(visionEnemy, 0, visionEnemy, screenHeight, RED);
                    DrawText (FormatText("%i", vida), 90, 0, 40, GREEN);
                    DrawText (FormatText("%i", vida2), 920, 0, 50, GREEN);
                    //DrawRectangleRec(recCentral2, col);
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    DrawText("GAME HAS FINISHED!", 0, 400, 50, BLACK);
                    DrawText("Press R to play again!", 0, 500, 30, RED);
                    DrawText("Press ESC to exit.", 0, 0, 30, RED);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
        UnloadMusicStream(musicPong);
        CloseAudioDevice();
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}